 Welcome to my MatrixTask project.
 
 This is the project for "The Second Development Challenge".

  @Description: 
 The main purpose of this priject is to describe
and represent the graph alghoritms of search the way in Matrix.
 It is small web site where user can set the task parameters,
start requested algorithm and see as it works in real time.

Version: 1.1.0

  @Structure: 
 Project runs on local WAMP (Apache, MySQL, PHP) server, with using 
Smarty web template system to help devide Front End and Back End side 
of the project.
 The main working directories are backEnd where located all .php files,
frontEnd where located all user interation parts: css, js directories, 
and templates directory where saved all .tpl files with html code.

  @ Instalation guide:
 To work with project localy you should have local  WAMP server on your machine.
The guidlines to install it you can find here: https://www.youtube.com/watch?v=yWstRAlAX1A&list=PLS1QulWo1RIZwGGYH4pm-WitfAUID_ZeR
 After it is installed you can save this project to you wamp/www/ folder.
 To be able run it you need to correct the paths so they lead to the correct 
files on your machine in files: mainWindow.php, matrixWindow.php

 @Demo:
The demo version is hosting on AWS, you can find it over 
the next link: http://13.59.201.21/MatrixTask/libs/backEnd/mainWindow.php

