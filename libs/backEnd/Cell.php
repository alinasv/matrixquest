<?php

/*
  @class Cell - main class which creates Matrix;
                main data structure is 2D array where
                each numeric row and column represents
                a separate Matrix cell;
*/

class Cell {

  /*
    @fields:

    $X: matrix size - given by user input;
        @type: Integer;

    $sparse: matrix sparse - given by user input;
             @type: Integer;

    $matrix: represents the matrix where each cell has values 0 or 1;
             @type: 2D array;           
    
    $matrixKeys: represents the matrix where each cells have different values
                 to represent different types of the cell: START, END,
                 black with '0' value, red with '1' value;
                 has the same size as $matrix;
                 @type: 2D array;

    $counter: value for incremental tracking; 
              @type: Integer;

  */

     private $X;
     private $sparse;
     private $matrix = array();
     private $matrixKeys = array();

     private $counter;
   
   /*
     Swap keys and values for passed $array with size $X in random order

     @param array $array and Integer $X;
     @return shuffled array;

   */
     private function randomShuffle($array,$X){
        
        for($i = 0; $i < $X; $i++){
          for($j = 0; $j < $X; $j++){
              $row = rand(0, $X-1);
              $col = rand(0, $X-1);

                  $temp = $array[$i][$j];
                  $array[$i][$j] = $array[$row][$col];
                  $array[$row][$col] = $temp;
          }
        }

        return $array;
     } 

       /*
        Calculate an abscissa for passed double number;

        @param: Float(double) $number;
        @return: first digit of $number and minus 1 as 
                 count starts from 0;
       */
       private function getCoordX($number){
             return floor($number)-1;
       }
       
       /*
        Calculate an ordinate for passed double number;

        @param: Float(double) $number;
        @return: second digit of $number and minus 1 as 
                 count starts from 0;
       */
       private function getCoordY($number){
             return ($number % 10) - 1;
       }


  
        /*
         Constructor. Creates arrays for Matrix and fill it with needed values.
         Defines all the fields.

         @param: Integer $X, $sparse,
                 Float $start, $end;

        */
       public function __construct($X, $sparse, $start, $end){

        /*
         @Definition  of all class fields;
        */
          $this->X = $X;
          $this->sparse = $sparse;
          $this->matrixKeys = array();
          $this->counter = 0; 

        
        /*
         @Define 'x' and 'y' for START and END coordinates;
        */
          $startX = $this->getCoordX($start);
          $startY = $this->getCoordY($start);
          $endX = $this->getCoordX($end);
          $endY = $this->getCoordY($end);

        
        /*
         @Define $perSent - the number of open and closed cells, 
          depending on the percentage of passed by user @sparse value;
          Defines by formula: Matrix Size - Matrix Size * sparse %;

        */
            $perSent=0;
            if($sparse == 1){
               (float)$perSent = round($X-($X*($sparse/100)))-1;          
            }
            else if($sparse == 100){
               (float)$perSent = round($X-($X*($sparse/100)))+1; 
            }
            else {
              (float)$perSent = round($X-($X*($sparse/100)));
            }
         
             /*
              @Fill arrays; 
              $matrix in foreach loop fills at $sparse % of 0
              and the rest is '1'. After $matrix filled it is shuffled;
              The cells which represent START and END cooordinates are
              given values 0; 


              @matrixKeys filled with increment values starts from 1;
               The cells which represent START and END cooordinates are
               given values 'A-2' for START and 'A-1' for END;
             */
              foreach (range(0,$X-1) as $row) {
               foreach (range(0,$X-1) as $col) {
                 if($row < $perSent){
                    $this->matrix[$row][$col] = 0;
                   }
                 else { 
                    $this->matrix[$row][$col] = 1;
                  }

                 $this->matrixKeys[$row][$col] = ++$this->counter;
                 
               }  
             }
               
               $this->matrix = $this->randomShuffle($this->matrix, $X);

               $this->matrix[$startX][$startY] = 0;
               $this->matrix[$endX][$endY] = 0;
             
               $this->matrixKeys[$startX][$startY] = 'A'.(-2);
               $this->matrixKeys[$endX][$endY] = 'A'.(-1);

       }

          /*
           Methods to get the fields: $X - matrix size; $sparse and $counter;    
          */
          private function getCounter(){
            return $this->counter;
          }

          public function getMxSize(){
            return $this->X;
          }
         
          public function getSparse(){
            return $this->sparse;
          } 


          /*
           Get value by passed coordinates;
           @param: Integer $i and Integer $j 
           @return:value of cell in $matrix by given coordinates;
          */
          public function getVal($i,$j){
            return $this->matrix[$i][$j];
          }
          
          /*
           Get value by passed coordinates;
           @param: Integer $i and Integer $j 
           @return:value of cell in $matrixKeys by given coordinates;
          */
          public function getKey($i,$j){
            return $this->matrixKeys[$i][$j]; 
          }
          


          /*
            Boolean functions to check whether the given by coordinates cell is START or END point
            @param: Integer $i and Integer $j 
            @return: Boolean value;S
          */
          private function isStartCoord($i, $j){
            if($this->matrixKeys[$i][$j] == 'A'.(-2))
               return true;
            else
               return false;
          }

          private function isEndCoord($i, $j){
            if($this->matrixKeys[$i][$j] == 'A'.(-1))
               return true;
            else
               return false;
          }


          /*
            Boolean function to check whether the given by coordinates cell 
            is blocked - has value '1'in matrix;
            @param: Integer $i and Integer $j' 
            @return: Boolean value;
          */
          private function isBlocked($i,$j){
            if($this->matrix[$i][$j] == 1)
               return true;
            else if ($this->matrix[$i][$j] == 0)
               return false;
          }


          /*
            Change value in cell by given coordinates unless it is START or END point;
            @param: Integer $i and Integer $j'; 
          */
          public function changeValue($i, $j){
            if(!($this->isStartCoord($i,$j)) && !($this->isEndCoord($i,$j)) &&  $this->matrix[$i][$j] == 1){
              $this->matrix[$i][$j] = 0;
              echo 0;  
              }
            else if(!($this->isStartCoord($i,$j)) && !($this->isEndCoord($i,$j)) &&  $this->matrix[$i][$j] == 0){
                 $this->matrix[$i][$j] = 1; 
                 echo 1; 
              }
            else {
              $this->matrix[$i][$j] = 0;
            }
          }

  
        /* 
         Help functions to print both arrays values;
        */
        private function show(){ 
            for($i = 0; $i < 3; $i++){
                    for($j = 0; $j < 3; $j++){
                         echo $this->matrix[$i][$j]." ";
                   }
              }
            echo " ";
          }

        private function showKeys(){
            for($i = 0; $i < count($this->matrixKeys); $i++){
                    for($j = 0; $j < ($this->matrixKeys); $j++){
                         echo $this->matrixKeys[$i][$j]." ";
                   }
              }
            echo " ";
          }
        }
