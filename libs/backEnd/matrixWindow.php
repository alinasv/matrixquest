<?php
/*
  @ File which displays data from the matrixWindow.tpl file; 
    Manipulate with matrix and its cells; 

  
  @At first it connects Smatry class and define files where all templates
   are generated.
*/
require 'D:\Wamp\wamp\www\MatrixTask\libs\smarty_classes\Smarty.class.php';
require 'D:\Wamp\wamp\www\MatrixTask\libs\backEnd\Cell.php';
require 'D:\Wamp\wamp\www\MatrixTask\libs\backEnd\Matrix.php';

$smarty = new Smarty();
$smarty -> template_dir = 'D:\Wamp\wamp\www\MatrixTask\libs\templates';
$smarty -> compile_dir = 'D:\Wamp\wamp\www\MatrixTask\libs\templates_c';

    /*
     Define head values
    */
    $title = "Matrix Window";

    /*
     From session takes values of user input in mainWindow.php and assign
     them to new variables $size, $sparse, $start, $end which are needed 
     as parameters to create new Matrix;
    */
    session_start();
    $size = $_SESSION['matrixSize'];
    $sparse = $_SESSION['matrixSparse'];
    $start = $_SESSION['start'];
    $end = $_SESSION['end'];

    $_SESSION["colNum"] = $size;

    /*
     @Creates $grid - new instance of class Cell; 
    */
    $grid = new Cell($size,$sparse,$start,$end);

    $count = $grid->getMxSize();


    /*
     @Created two 2D arrays which filles the matrix value; 
      the first $boolValue - get '0' and '1' values from the created $gridl
   
      The second $cellsID is field with matrixKeys values which will represent
      the "id" tag of matrix; 
    */
    $boolValue = array();
    $cellsID = array();

        for($i = 0; $i < $count; ++$i){
            for($j = 0; $j < $count; ++$j){
                $boolValue[$i][$j] = $grid->getVal($i,$j); 
                $cellsID[$i][$j] = $grid->getKey($i,$j);
             }
          }

    /*
      from changeVal(val,val2, item) js function with ajax help
      gets the data of coordinates the clicked by user cell and 
      changes its value in $grid;
    */
    if(count($_POST) > 0){
        $a = $_POST['a'];
        $b = $_POST['b'];
        return $grid->changeValue($a,$b);
    }

    
    /*
    @Smarty assign all the variables and transfer them to 
    and display with matrixWindow.tpl   
    */
    $smarty -> assign("title", $title);
    $smarty -> assign("count", $count);
    $smarty -> assign("boolValue", $boolValue);
    $smarty -> assign("cellsID", $cellsID);

    $smarty -> display("matrixWindow.tpl");
