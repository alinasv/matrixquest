<?php header("Content-type: text/css; charset: UTF-8"); 

session_start();
$size = $_SESSION['matrixSize'];

?>


#content{
	//background-color: bisque;
  background-color: #444;
  color: #2C75FF;
  border: 1px solid #ddd;
  margin: 10px;
	padding-top: 10px;
	padding-bottom: 10px;
	font-weight: bold;
  font-family: "Georgia";
}

#paramLabel{
  text-align: center;
  font-family: "Georgia";
}


#maxtrix{
  border: 1px solid #ddd;
  display: grid;
  grid-template-columns: repeat(<?php echo $size;?>, 30px);
  grid-template-rows: repeat(<?php echo $size;?>, 30px);
  grid-gap: 2px;
  background-color: #fff;
  color: #444;
    

  padding-top:10px;
  padding-bottom:10px;
  padding-left:10px;
  padding-right:10px;
  justify-content: center;
}


.cell {
	  background-color: #444; 
    color: #fff;
    border: 1px #ddd;
    border-radius: 2px;
    padding: 1px;
    font-size: 100%;
    text-align: center;
    font-family: "Georgia";
}

#A-2  {
  //background-color: blue;
  background-color: #2C75FF;
}

#A-1  {
  //background-color: green;
  background-color: #2C75FF;;
}


#btnRow {
  position: relative;
  padding-left:45%;
  padding-right:45%;

  padding-top:10px;
  padding-bottom:10px;
}