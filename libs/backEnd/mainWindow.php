 <?php
/*
  @ File which displays data from the mainWindow.tpl file 
    and transfer user input infromation form it to the matrixWindow.php
    for next manipulations 

  
  @At first it connects Smatry class and define files where all templates
   are generated.
*/
 
require 'D:\Wamp\wamp\www\MatrixTask\libs\smarty_classes\Smarty.class.php';
$smarty = new Smarty();
$smarty -> template_dir = 'D:\Wamp\wamp\www\MatrixTask\libs\templates';
$smarty -> compile_dir = 'D:\Wamp\wamp\www\MatrixTask\libs\templates_c';


$title = "Main Window";
$smarty -> assign("title", $title);

/*
  @In session variables are stored input data like
   size of the matrix (X), matrix sparse (S), values of 
   the first (START) and last (END) coordinates;
   
  @After all needed data colleacted its sent and reloaded
   to the matrixWindow.php page.
*/
session_start();
if(count($_POST) > 0){

    $_SESSION['matrixSize'] = $_POST["matrixSize"];
    $_SESSION['matrixSparse'] = $_POST["matrixSparse"];
    $_SESSION['start'] = $_POST["start"];
    $_SESSION['end'] = $_POST["end"];

	header("Location: matrixWindow.php");
}

$smarty -> display("mainWindow.tpl");

?>