<?php
/* Smarty version 3.1.31, created on 2018-09-19 17:03:53
  from "D:\Wamp\wamp\www\MatrixTask\libs\templates\matrixWindow.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ba265591a5e48_44622067',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7d28bc646b3bc642017fa136363255c58c7367dc' => 
    array (
      0 => 'D:\\Wamp\\wamp\\www\\MatrixTask\\libs\\templates\\matrixWindow.tpl',
      1 => 1537367556,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba265591a5e48_44622067 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang = "eng">
     <head>
	   <meta charset = "utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <meta http-equiv="x-ua-compatible" content="ie=edge">
		
       <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

		<!-- Include Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		

		<!-- Include styles file 
		<link rel = "stylesheet" href = "/libs/frontEnd/css/style.css">
    -->
    <link rel = "stylesheet" href = "styles.php">
	</head>

    <body>
         <div class = "container" id = "maxtrix">
             <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['count']->value-1+1 - (0) : 0-($_smarty_tpl->tpl_vars['count']->value-1)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                <?php
$_smarty_tpl->tpl_vars['j'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['j']->step = 1;$_smarty_tpl->tpl_vars['j']->total = (int) ceil(($_smarty_tpl->tpl_vars['j']->step > 0 ? $_smarty_tpl->tpl_vars['count']->value-1+1 - (0) : 0-($_smarty_tpl->tpl_vars['count']->value-1)+1)/abs($_smarty_tpl->tpl_vars['j']->step));
if ($_smarty_tpl->tpl_vars['j']->total > 0) {
for ($_smarty_tpl->tpl_vars['j']->value = 0, $_smarty_tpl->tpl_vars['j']->iteration = 1;$_smarty_tpl->tpl_vars['j']->iteration <= $_smarty_tpl->tpl_vars['j']->total;$_smarty_tpl->tpl_vars['j']->value += $_smarty_tpl->tpl_vars['j']->step, $_smarty_tpl->tpl_vars['j']->iteration++) {
$_smarty_tpl->tpl_vars['j']->first = $_smarty_tpl->tpl_vars['j']->iteration == 1;$_smarty_tpl->tpl_vars['j']->last = $_smarty_tpl->tpl_vars['j']->iteration == $_smarty_tpl->tpl_vars['j']->total;?>
              <div class = "cell" id = "<?php echo $_smarty_tpl->tpl_vars['cellsID']->value[$_smarty_tpl->tpl_vars['i']->value][$_smarty_tpl->tpl_vars['j']->value];?>
" onclick="changeVal('<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
', '<?php echo $_smarty_tpl->tpl_vars['j']->value;?>
', this);"><?php echo $_smarty_tpl->tpl_vars['boolValue']->value[$_smarty_tpl->tpl_vars['i']->value][$_smarty_tpl->tpl_vars['j']->value];?>
</div>
                 <?php }
}
?>

             <?php }
}
?>

                   
         </div> 

         <div class="btn-group btn-group-justified" id = "btnRow">
              <button type="submit" class="btn btn-info btn-block" id="gridBtn">Continue search</button>
          </div>

         <!-- jQuery JavaScript -->
<?php echo '<script'; ?>
 type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"> <?php echo '</script'; ?>
>
<!--Bootsrap JavaScript -->
<?php echo '<script'; ?>
 src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<!--Own JavaScript -->
<?php echo '<script'; ?>
 type="text/javascript" src="js.php"> <?php echo '</script'; ?>
>

    </body>

    <!-- 
      @front end script to manipulate with matrix
    -->
    <?php echo '<script'; ?>
 type="text/javascript">
         $(function(){
       
           /* 
             @ Cover each cell which has value '1' to red color
            */     
           function changeToRed(){
              $(".cell").each(function(){
            
                if($(this).text() == "1"){
                   $(this).css("background-color","red");
                }
               });
            }

           changeToRed();
     });

            /* 
             @After click change value of this cell to '0' if orinal value 
              was '1' or vice versa 
              Additionaly if the value changed from '1' to '0' the color of this
              cell changes to dark and to red if the value becomes '1';

             @param: String $val and $val2 which are cells coordinated of x and y;
                     object item - the item which color should be changed. In this
                     case it is the clicked cell which is refers to itself (this) after click;

             @ajax is used to send data to matrixWindow.php and with function changeValue() 
                   check and change value of needed cell;

             @validation: before changes check whether the cell is not START of END as such
                          shoudl not be changed; 
            */
          function changeVal(val,val2, item){   
            $.ajax({
              url:'/MatrixTask/libs/backEnd/matrixWindow.php',
              type: 'POST',
              dataType:"text", 
              data: { a: val, b: val2},
              success: function(responce){
                  if($(item).attr('id') !== "A-2" && 
                     $(item).attr('id') !== "A-1" && 
                     $(item).text() == "0"){
                                            $(item).text(responce);
                                            $(item).css("background-color","red");
                  }

                  else if($(item).attr('id') !== "A-2" && 
                          $(item).attr('id') !== "A-1" && 
                          $(item).text() == "1"){
                                            $(item).text(responce);
                                            $(item).css("background-color","#444");
                  }
              }
            });
            return false;
          }
        <?php echo '</script'; ?>
>

</html><?php }
}
