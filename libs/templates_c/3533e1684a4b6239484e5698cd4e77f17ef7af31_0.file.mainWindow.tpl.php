<?php
/* Smarty version 3.1.31, created on 2018-09-19 01:43:24
  from "D:\Wamp\wamp\www\MatrixTask\libs\templates\mainWindow.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ba18d9cc89fd2_93563877',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3533e1684a4b6239484e5698cd4e77f17ef7af31' => 
    array (
      0 => 'D:\\Wamp\\wamp\\www\\MatrixTask\\libs\\templates\\mainWindow.tpl',
      1 => 1537314200,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ba18d9cc89fd2_93563877 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>

	<head lang = "en">
	   <meta charset = "utf-8">
		
       <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

		<!-- Include Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

		<!-- Include styles file
		<link rel="stylesheet" type="text/css" href="/libs/frontEnd/css/style.css">
	    -->

	    <link rel = "stylesheet" href = "styles.php">
		
	</head>
	
	<body>
	  
		<div class = "container" >
			<div class = "row row-content justify-content-center">
				<div class = "col-lg-4" id = "content">
					
					 <p class="h4 mb-4"><h3 id = "paramLabel">Parameters</h3></p>
					<form action = "mainWindow.php" method = "POST">
						<!-- Size -->
						<div class = "form-group">
							 <label for="matrixSize">Matrix Size (X)</label>
                             <input type = "number" name = "matrixSize" class="form-control" min="1" max="100" value = "25">
                             <span class="validity"></span>
					    </div>	
						
						
						<!-- Sparse -->
							<div class = "form-group">
								<label for="matrixSparse">Matrix Sparse (S)</label>
								<input type = "number" name = "matrixSparse" class="form-control" min="1" max="100" value = "30">
                                <span class="validity"></span>
							</div>	
						
						
						<!-- Search Algorithm -->
							<div class = "form-group">
								<label for="searchAlgorithm">Search Algorithm (S)</label>
								<select class="form-control" id="searchAlgorithm">
									  <option>Depth-first search</option>
									  <option>Breadth-first search</option>
								</select>
							</div>	
						
						<!-- Animation speed -->
							<div class = "form-group">
								<label for="matrixSize">Animation speed(A)</label>
								<input type="number" class="form-control" data-bind="value:replyNumber" value="100">
							</div>	
						
						<!-- Start coordinate -->
							<div class = "form-group">
								<label for="matrixSize">Start coordinate (START)</label>
								<input type="number" step = "any" name = "start"class="form-control" data-bind="value:replyNumber" value="5.1">
							</div>	
					
						
						<!-- End coordinat -->
							<div class = "form-group">
								<label for="matrixSize">End coordinate (END)</label>
								<input type="number" step = "any" name = "end" class="form-control" data-bind="value:replyNumber" value="1.5">
							</div>	
						
						<!-- Step method -->
							<div class = "form-group">
								<label for="matrixSize">Step method (M)</label>
								<select class="form-control" id="stepMethod" value = "4 steps">
									  <option>4 steps</option>
									  <option>8 steps</option>
								</select>
							</div>	

						
						<!-- Step strategy -->
							<div class = "form-group">
								<label for="matrixSize">Step strategy (STRATEGY)</label>
								<select class="form-control" id="stepStrategy" value = "4 steps">
									  <option>Сlockwise starting from top</option>
									  <option>Сounterclock-wise starting from top</option>
									  <option>Randomly</option>
								</select>
							</div>	
						
						
						<!--for buttons-->
						 <button type="submit" class="btn btn-info btn-block">Start search</button> 
			
					</form>
				</div>
			</div>
		</div>
		
	</body>

</html><?php }
}
