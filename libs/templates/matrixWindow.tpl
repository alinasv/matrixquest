<!DOCTYPE html>
<html lang = "eng">
     <head>
	   <meta charset = "utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
       <meta http-equiv="x-ua-compatible" content="ie=edge">
		
       <title>{$title}</title>

		<!-- Include Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
		

		<!-- Include styles file 
		<link rel = "stylesheet" href = "/libs/frontEnd/css/style.css">
    -->
    <link rel = "stylesheet" href = "styles.php">
	</head>

    <body>
         <div class = "container" id = "maxtrix">
             {for $i = 0 to $count - 1}
                {for $j = 0 to $count - 1 }
              <div class = "cell" id = "{$cellsID[$i][$j]}" onclick="changeVal('{$i}', '{$j}', this);">{$boolValue[$i][$j]}</div>
                 {/for}
             {/for}
                   
         </div> 

         <div class="btn-group btn-group-justified" id = "btnRow">
              <button type="submit" class="btn btn-info btn-block" id="gridBtn">Continue search</button>
          </div>

         <!-- jQuery JavaScript -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"> </script>
<!--Bootsrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<!--Own JavaScript -->
<script type="text/javascript" src="js.php"> </script>

    </body>

    <!-- 
      @front end script to manipulate with matrix
    -->
    <script type="text/javascript">
         $(function(){
       
           /* 
             @ Cover each cell which has value '1' to red color
            */     
           function changeToRed(){
              $(".cell").each(function(){
            
                if($(this).text() == "1"){
                   $(this).css("background-color","red");
                }
               });
            }

           changeToRed();
     });

            /* 
             @After click change value of this cell to '0' if orinal value 
              was '1' or vice versa 
              Additionaly if the value changed from '1' to '0' the color of this
              cell changes to dark and to red if the value becomes '1';

             @param: String $val and $val2 which are cells coordinated of x and y;
                     object item - the item which color should be changed. In this
                     case it is the clicked cell which is refers to itself (this) after click;

             @ajax is used to send data to matrixWindow.php and with function changeValue() 
                   check and change value of needed cell;

             @validation: before changes check whether the cell is not START of END as such
                          shoudl not be changed; 
            */
          function changeVal(val,val2, item){   
            $.ajax({
              url:'/MatrixTask/libs/backEnd/matrixWindow.php',
              type: 'POST',
              dataType:"text", 
              data: { a: val, b: val2},
              success: function(responce){
                  if($(item).attr('id') !== "A-2" && 
                     $(item).attr('id') !== "A-1" && 
                     $(item).text() == "0"){
                                            $(item).text(responce);
                                            $(item).css("background-color","red");
                  }

                  else if($(item).attr('id') !== "A-2" && 
                          $(item).attr('id') !== "A-1" && 
                          $(item).text() == "1"){
                                            $(item).text(responce);
                                            $(item).css("background-color","#444");
                  }
              }
            });
            return false;
          }
        </script>

</html>