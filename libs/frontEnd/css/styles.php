<?php header("Content-type: text/css; charset: UTF-8"); 
?>

#content{
  background-color: bisque;
  padding-top: 10px;
  padding-bottom: 10px;
  font-weight: bold;
}


#maxtrix{
  border: 1px solid #ddd;
  display: grid;
  grid-template-columns: repeat(5, 30px);
  grid-template-rows: repeat(5, 30px);
  grid-gap: 2px;
  background-color: #fff;
  color: #444;
    

  padding-top:10px;
  padding-bottom:10px;
  padding-left:10px;
  padding-right:10px;
  justify-content: center;
}


.cell {
  background-color: #444;
    color: #fff;
    border-radius: 2px;
    padding: 1px;
    font-size: 100%;
    text-align: center;
}
